
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x02, 0x2f, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xb5, 0x96, 0xcf, 0x6b, 0x1a,
 0x41, 0x14, 0xc7, 0x25, 0x94, 0x1e, 0x92, 0x7f, 0x22, 0xed, 0xa1, 0x29, 0x44, 0x02, 0xbd, 0xb7,
 0x89, 0x11, 0x92, 0x9b, 0x07, 0x7f, 0x55, 0x8b, 0xf5, 0x2f, 0x50, 0x48, 0x13, 0xc1, 0x43, 0x0e,
 0x21, 0x3f, 0x5a, 0xf0, 0x14, 0x2a, 0xc5, 0xff, 0xc0, 0xb3, 0x42, 0x0e, 0x39, 0x09, 0xcb, 0x22,
 0x68, 0xed, 0xae, 0x95, 0x6c, 0xbb, 0x11, 0x43, 0x3c, 0x78, 0x4c, 0x40, 0x63, 0x84, 0x10, 0x53,
 0x75, 0x5f, 0xde, 0xdb, 0x64, 0x64, 0x95, 0xf5, 0xf7, 0xf6, 0xc1, 0x63, 0x9c, 0xdd, 0x99, 0xef,
 0x67, 0xde, 0xbc, 0x37, 0xb3, 0x9a, 0x4c, 0x1a, 0x73, 0x3a, 0x9d, 0x3f, 0x5c, 0x2e, 0xd7, 0x3d,
 0xfa, 0xae, 0xc5, 0x62, 0x79, 0x61, 0x32, 0xda, 0x50, 0x38, 0x85, 0x0e, 0xcf, 0x2e, 0xd8, 0xed,
 0x76, 0xf3, 0x7f, 0x01, 0xf8, 0x7c, 0x3e, 0x06, 0x69, 0x1a, 0x1a, 0x0d, 0x03, 0x08, 0x82, 0x00,
 0xf1, 0x78, 0x1c, 0xdc, 0x6e, 0x37, 0x03, 0x9d, 0xe1, 0xf6, 0xbd, 0x9b, 0x56, 0x34, 0xad, 0xd9,
 0x16, 0xd5, 0x25, 0x49, 0x02, 0xb2, 0x62, 0xb1, 0x08, 0xc1, 0x60, 0x90, 0x3d, 0xff, 0x87, 0x1e,
 0x41, 0xe8, 0x4b, 0xc3, 0x00, 0x64, 0xcd, 0x66, 0xd3, 0xd0, 0x68, 0x52, 0xfd, 0x00, 0x66, 0x46,
 0x45, 0xd3, 0x05, 0x70, 0x1c, 0xd7, 0x13, 0x55, 0x36, 0x9b, 0x9d, 0x3d, 0x9a, 0x61, 0x11, 0x18,
 0x12, 0xcd, 0xb8, 0x80, 0xa9, 0x73, 0x33, 0x09, 0x80, 0x59, 0xab, 0xd5, 0x82, 0x76, 0xbb, 0x0d,
 0xb5, 0x5a, 0x0d, 0x44, 0x51, 0x54, 0x62, 0xb1, 0xd8, 0x35, 0x42, 0x45, 0xd4, 0x21, 0x4f, 0x4c,
 0x0d, 0xa8, 0x0b, 0x12, 0xc8, 0x5f, 0x0e, 0x21, 0xf3, 0xde, 0x05, 0xfc, 0xf2, 0x06, 0x64, 0x3e,
 0xb8, 0x41, 0xde, 0x3e, 0x82, 0xba, 0x28, 0x41, 0xb9, 0x5c, 0x86, 0x40, 0x20, 0x40, 0x51, 0x75,
 0x46, 0x02, 0x78, 0x9e, 0xef, 0x11, 0x56, 0x70, 0xb5, 0xa5, 0xbd, 0x63, 0xe0, 0xde, 0xac, 0x0f,
 0xf4, 0x8b, 0xfd, 0xef, 0xd0, 0x7a, 0x78, 0xa8, 0x26, 0x12, 0x89, 0xb5, 0x91, 0x00, 0xea, 0x6b,
 0x4d, 0x4f, 0xfc, 0xe6, 0x67, 0x41, 0x17, 0x82, 0x26, 0x0e, 0x04, 0xa4, 0xd3, 0xe9, 0x6e, 0x89,
 0x52, 0x22, 0x4b, 0xa5, 0x12, 0xdc, 0xfc, 0x3a, 0xeb, 0x0a, 0x54, 0xf9, 0x1c, 0x14, 0x3e, 0x6f,
 0xab, 0xbf, 0xc9, 0xa8, 0xa5, 0x3e, 0x3d, 0x67, 0x63, 0xea, 0xf9, 0x3f, 0x30, 0x76, 0x04, 0x8a,
 0xa2, 0x80, 0xbc, 0x75, 0xd0, 0x9d, 0x4c, 0x62, 0xf5, 0xdf, 0x7f, 0x81, 0x7b, 0x6b, 0x7d, 0x02,
 0x60, 0x4b, 0x7d, 0x06, 0x25, 0x97, 0x77, 0x8e, 0x60, 0xe4, 0x55, 0x91, 0xcb, 0xe5, 0xa0, 0x52,
 0xa9, 0x40, 0xa3, 0xd1, 0x50, 0x13, 0xda, 0xb3, 0x15, 0x28, 0xaa, 0x8d, 0x80, 0xf5, 0x99, 0x53,
 0xe2, 0x87, 0x02, 0xa8, 0xfc, 0x92, 0xc9, 0x24, 0x78, 0xbd, 0x5e, 0xb5, 0x1c, 0xa9, 0x5a, 0xd8,
 0x9e, 0x0f, 0x33, 0x96, 0x13, 0xde, 0xbc, 0x39, 0x78, 0x8b, 0x68, 0xd5, 0xe1, 0x70, 0x98, 0x41,
 0x95, 0x3b, 0xb4, 0x89, 0x23, 0x58, 0xfd, 0xa8, 0x0f, 0x88, 0x44, 0x22, 0xe0, 0xf1, 0x78, 0x54,
 0x71, 0x3c, 0x9d, 0x97, 0x0e, 0x87, 0x63, 0x15, 0x35, 0x4e, 0xa8, 0xce, 0x27, 0xc9, 0xc1, 0x79,
 0xe8, 0x1b, 0x0c, 0xfb, 0x64, 0x76, 0xd0, 0xa3, 0x36, 0x9b, 0x6d, 0x9e, 0xde, 0xa1, 0xc6, 0xa7,
 0xdb, 0x82, 0x0c, 0xdc, 0x92, 0x75, 0xa6, 0x2a, 0x3a, 0xd5, 0xae, 0x5a, 0xfb, 0x0e, 0x35, 0xe6,
 0xd0, 0xf3, 0x54, 0xdf, 0x63, 0x9d, 0x83, 0xc3, 0x28, 0x71, 0x05, 0x53, 0xdf, 0xbf, 0x8a, 0x15,
 0x04, 0x84, 0xfc, 0x7e, 0xff, 0x82, 0xde, 0x5d, 0x85, 0x13, 0x5e, 0xe3, 0x49, 0xbe, 0xd2, 0x83,
 0xf4, 0x8b, 0xd3, 0x38, 0x1c, 0xbf, 0x38, 0xf1, 0x47, 0x09, 0x27, 0xbd, 0xa2, 0x13, 0x4a, 0xe1,
 0x53, 0x9d, 0x53, 0x22, 0xd5, 0xbb, 0x08, 0xdb, 0xf3, 0xd0, 0x57, 0x35, 0x0f, 0xb4, 0x72, 0x1a,
 0x47, 0xe3, 0x1f, 0x01, 0xf7, 0x32, 0x1d, 0x17, 0xbe, 0x4a, 0x46, 0xe9, 0x00, 0x00, 0x00, 0x00,
 0x49, 0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE new_component_xpm[1] = {{ png, sizeof( png ), "new_component_xpm" }};

//EOF
